package com.dubilok.service;

import com.dubilok.model.callable_fibonacci.CallableFibonacci;
import com.dubilok.model.ping_pong.PingPong;
import com.dubilok.model.fibonacci.Fibonacci;
import com.dubilok.model.pipe_communication.PipeCommunication;
import com.dubilok.model.sleep_scheduled_thread_pool.SleepThread;
import com.dubilok.model.three_methods.OneSynchronized;
import com.dubilok.model.three_methods.ThreeSynchronized;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ManagerImpl implements Manager {

    private static Logger logger = LogManager.getLogger(ManagerImpl.class);

    @Override
    public void showTaskFirst() {
        new PingPong();
    }

    @Override
    public void showTaskSecond() {
        for (int i = 1; i <= 10; i += 2) {
            final int n = i;
            Thread thread = new Thread(() -> logger.info(Thread.currentThread().getName() + "; value = " + n
                    + "; result = " + new Fibonacci().fibonacci(n)));
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                logger.error(e);
            }
        }
    }

    @Override
    public void showTaskThird() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        for (int i = 4; i < 15; i+=2) {
            final int n = i;
            executorService.execute(() -> logger.info(Thread.currentThread().getName() + "; value = " + n
                    + "; result = " + new Fibonacci().fibonacci(n)));
        }
        executorService.shutdown();
    }

    @Override
    public void showFourthTask() {
        new CallableFibonacci();
    }

    @Override
    public void showTaskSixth() {
        new OneSynchronized();
        new ThreeSynchronized();
    }

    @Override
    public void showTaskSeventh() {
        new PipeCommunication();
    }

    @Override
    public void showTaskFifth() {
        new SleepThread();
    }

}
