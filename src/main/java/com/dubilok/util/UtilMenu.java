package com.dubilok.util;

import com.dubilok.model.three_methods.OneSynchronized;
import com.dubilok.view.Printable;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

public class UtilMenu {
    private static void outputMenu(Map<String, String> menu) {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public static void show(BufferedReader bufferedReader, Map<String, String> menu, Map<String, Printable> methodsMenu) {
        String keyMenu = null;
        do {
            outputMenu(menu);
            System.out.println("Please, select menu point.");
            try {
                keyMenu = bufferedReader.readLine().toUpperCase();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    public static void increment(Object object, OneSynchronized count, Logger logger) {
        synchronized (object) {
            for (int i = 0; i < 1_000; i++) {
                int increment = count.getCount() + 1;
                count.setCount(increment);
            }
        }
        logger.info(Thread.currentThread().getName() + "; count = " + count.getCount());
    }
}
