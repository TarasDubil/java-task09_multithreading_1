package com.dubilok.model.ping_pong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {

    private static Logger logger = LogManager.getLogger(PingPong.class);
    private static Object object = new Object();

    public PingPong() {
        start();
    }

    public static void start() {
        Ping ping = new Ping();
        Pong pong = new Pong();
        ping.start();
        pong.start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            logger.error(e);
        }
        ping.interrupt();
        pong.interrupt();
    }

    static class Ping extends Thread {
        @Override
        public void run() {
            synchronized (object) {
                while (!Thread.interrupted()) {
                    try {
                        object.wait();
                        logger.info("Ping");
                        object.notify();
                    } catch (InterruptedException e) {
                        logger.error(e);
                    }
                }
            }
        }
    }

    static class Pong extends Thread {
        @Override
        public void run() {
            synchronized (object) {
                while (!Thread.interrupted()) {
                    object.notify();
                    logger.info("Pong");
                    try {
                        object.wait();
                    } catch (InterruptedException e) {
                        logger.error(e);
                    }
                }
            }
        }
    }

}
