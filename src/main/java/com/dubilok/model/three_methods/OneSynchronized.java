package com.dubilok.model.three_methods;

import com.dubilok.util.UtilMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OneSynchronized {

    private volatile int count;
    private final Object object = new Object();
    private Logger logger = LogManager.getLogger(OneSynchronized.class);

    public OneSynchronized() {
        logger.info("One Synchronized");
        Thread threadFirst = new Thread(this::incrementFirst);
        Thread threadSecond = new Thread(this::incrementSecond);
        Thread threadThird =  new Thread(this::incrementThird);
        threadFirst.start();
        threadSecond.start();
        threadThird.start();
        try {
            threadFirst.join();
            threadSecond.join();
            threadThird.join();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    public synchronized int getCount() {
        return count;
    }

    public synchronized void setCount(int count) {
        this.count = count;
    }

    private void incrementFirst() {
        UtilMenu.increment(object, this, logger);
    }

    private void incrementSecond() {
        UtilMenu.increment(object, this, logger);
    }

    private void incrementThird() {
        UtilMenu.increment(object, this, logger);
    }
}
