package com.dubilok.model.three_methods;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ThreeSynchronized {
    private volatile int countFirst;
    private volatile int countSecond;
    private volatile int countThird;
    private final Object objectFirst = new Object();
    private final Object objectSecond = new Object();
    private final Object objectThird = new Object();
    private Logger logger = LogManager.getLogger(OneSynchronized.class);

    public ThreeSynchronized() {
        logger.info("Three Synchronized");
        Thread threadFirst = new Thread(this::incrementFirst);
        Thread threadSecond = new Thread(this::incrementSecond);
        Thread threadThird =  new Thread(this::incrementThird);
        threadFirst.start();
        threadSecond.start();
        threadThird.start();
        try {
            threadFirst.join();
            threadSecond.join();
            threadThird.join();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    private void incrementFirst() {
        synchronized (objectFirst) {
            for (int i = 0; i < 1_000; i++) {
                this.countFirst++;
            }
        }
        logger.info(Thread.currentThread().getName() + "; countFirst = " + this.countFirst);
    }

    private void incrementSecond() {
        synchronized (objectFirst) {
            for (int i = 0; i < 1_000; i++) {
                this.countSecond++;
            }
        }
        logger.info(Thread.currentThread().getName() + "; countSecond = " + this.countSecond);
    }

    private void incrementThird() {
        synchronized (objectFirst) {
            for (int i = 0; i < 1_000; i++) {
                this.countThird++;
            }
        }
        logger.info(Thread.currentThread().getName() + "; countThird = " + this.countThird);
    }
}
