package com.dubilok.model.pipe_communication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ResourceBundle;

public class PipeCommunication {

    private static PipedInputStream pipedInputStream = new PipedInputStream();
    private static ResourceBundle constant = ResourceBundle.getBundle("constant");
    private static PipedOutputStream pipedOutputStream = new PipedOutputStream();
    private static Logger logger = LogManager.getLogger(PipeCommunication.class);
    private static volatile String data;

    {
        data = constant.getString("data");
    }

    public PipeCommunication() {
        start();
    }

    public void start() {
        try {
            pipedInputStream.connect(pipedOutputStream);
            Thread write = new PipeWrite();
            Thread read = new PipeRead();
            write.start();
            read.start();
            try {
                write.join();
                read.join();
            } catch (InterruptedException e) {
                logger.error(e);
            }
        } catch (IOException e) {
            logger.error(e);
        }
    }

    static class PipeWrite extends Thread {
        @Override
        public void run() {
            char[] array = data.toCharArray();
            logger.info("Written: " + data);
            for (Character c : array) {
                try {
                    pipedOutputStream.write(c);
                } catch (IOException e) {
                    logger.error(e);
                }
            }
            try {
                pipedOutputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
    }

    static class PipeRead extends Thread {
        @Override
        public void run() {
            logger.info("Read: ");
            int ch;
            try {
                while ((ch = pipedInputStream.read()) != -1) {
                    logger.info((char)ch);
                    Thread.sleep(500);
                }
                pipedInputStream.close();
            } catch (IOException | InterruptedException e) {
                logger.error(e);
            }
        }
    }
}
