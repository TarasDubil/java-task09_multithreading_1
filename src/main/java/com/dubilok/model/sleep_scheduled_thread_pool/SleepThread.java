package com.dubilok.model.sleep_scheduled_thread_pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SleepThread {

    private static Logger logger = LogManager.getLogger(SleepThread.class);

    public SleepThread() {
        start();
    }

    public void start() {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);
        execute(executorService);
        executorService.shutdown();
    }

    private void execute(ScheduledExecutorService executorService) {
        for (int i = 0; i < 10; i+=2) {
            int sleepTime = new Random().nextInt(10) + 1;
            executorService.schedule(() -> logger.info("Sleep time: " + sleepTime), sleepTime, TimeUnit.SECONDS);
        }
    }
}
