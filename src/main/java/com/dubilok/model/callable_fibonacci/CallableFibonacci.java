package com.dubilok.model.callable_fibonacci;

import com.dubilok.model.fibonacci.Fibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class CallableFibonacci {

    public CallableFibonacci() {
        start();
    }

    private static Logger logger = LogManager.getLogger(CallableFibonacci.class);

    public void start() {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        try {
            long sum = executorService.invokeAll(generateCallable()).stream()
                    .mapToLong(this::getValue)
                    .sum();
            logger.info("Sum = " + sum);
        } catch (InterruptedException e) {
            logger.error(e);
        }
        executorService.shutdown();
    }

    private long getValue(Future<Long> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            logger.error(e);
        }
        return 0L;
    }


    private List<Callable<Long>> generateCallable() {
        final int[] array = {4, 7, 12, 10, 20};
        return Arrays.stream(array)
                .mapToObj(value -> (Callable<Long>) () -> new Fibonacci().fibonacci(value))
                .collect(Collectors.toList());
    }

}
