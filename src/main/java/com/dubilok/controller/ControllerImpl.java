package com.dubilok.controller;

import com.dubilok.service.Manager;
import com.dubilok.service.ManagerImpl;

public class ControllerImpl implements Controller {
    private Manager manager;

    public ControllerImpl() {
        manager = new ManagerImpl();
    }

    @Override
    public void firstTask() {
        manager.showTaskFirst();
    }

    @Override
    public void secondTask() {
        manager.showTaskSecond();
    }

    @Override
    public void thirdTask() {
        manager.showTaskThird();
    }

    @Override
    public void fourthTask() {
        manager.showFourthTask();
    }

    @Override
    public void fifthTask() {
        manager.showTaskFifth();
    }

    @Override
    public void sixthTask() {
        manager.showTaskSixth();
    }

    @Override
    public void seventhTask() {
        manager.showTaskSeventh();
    }
}
